#!/usr/bin/env python3
import socket
import json

msg= 'A message in a Ü-tube'.encode()
udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_client.sendto(msg, ('127.0.0.1', 20001))
print('send "{}"'.format(msg.decode()))
data, server_address = udp_client.recvfrom(1024)
print('recv data from udp://{}:{}'.format(*server_address))
print('recv "{}"'.format(data.decode()))
print(json.loads(data.decode()))
