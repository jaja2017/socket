#!/usr/bin/env python3

# The socket server library is a more powerful module for handling sockets, it will help you set up and manage multiple clients in the next step
import socketserver
from collections import namedtuple
from fl_networking_tools import get_binary, send_binary
from threading import Event



class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

NUMBER_OF_PLAYERS = 2
players = []

ready_to_start = Event()




'''
Commands:
PLACE YOUR COMMANDS HERE
JOIN - adds team to plyers
QUES - ASK question command
EVAL - EVAL answer
'''

# Named tuples are extensions of the tuple structure, with contents you can refer to by name. In this case, the question will be held in a variable named q and the answer in answer.
# This is just the set up of the question - it will be sent to the client using the send_binary function when a request is made.
Question = namedtuple('Question', ['q', 'answer'])

q1 = Question("Expand the acronym ALU", "Arithmetic Logic Unit")

# The socketserver module uses 'Handlers' to interact with connections. When a client connects a version of this class is made to handle it.
class QuizGame(socketserver.BaseRequestHandler):
    # The handle method is what actually handles the connection
    def handle(self):
        global players # Make sure this is global
        #Retrieve Command
        for command in get_binary(self.request):
            if command[0] == "JOIN":
                players.append(command[1])
                print('Team {} joined.'.format(command[1]))
                if len(players) == NUMBER_OF_PLAYERS:
                    # If correct number of players
                    ready_to_start.set()
                    # Trigger the event
                # Send the confirmation response ##should use not the same No as Answer-OK
                # send_binary(self.request, [2, ""]) ##else don't know how to handle on client side 
                # Wait for the ready to start event
                ready_to_start.wait()
            if command[0] == "QUES":
                #Send question
                send_binary(self.request, (4, q1.q))
        #Your server code goes here
            elif command[0] == "EVAL":
                print(q1.answer.lower(), command[1].lower())
                if q1.answer.lower() == command[1].lower():
                    send_binary(self.request, (2,))
                else:
                    send_binary(self.request, (3,))
            elif command[0] == "BYE":
                print('Clinet said BYE.')
                break
        self.request.close()
# Open the quiz server and bind it to a port - creating a socket
# This works similarly to the sockets you used before, but you have to give it both an address pair (IP and port) and a handler for the server.
quiz_server = ThreadedTCPServer(('127.0.0.1', 2065), QuizGame)
quiz_server.serve_forever()