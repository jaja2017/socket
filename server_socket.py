#!/usr/bin/env python3
import socket

def send_text(sending_socket, text):
    text = text + "\n"
    data = text.encode()
    sending_socket.send(data)


''' AF_INET specifies Internet Protocol version 4 and SOCK_STREAM denotes TCP, so a TCP/IP socket is created'''
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
'''all network IP addresses available on this computer connecting the port - use 127.0.0.1 for only loopback access'''
server_socket.bind(("0.0.0.0", 8081))
server_socket.listen()
print("Waiting for connection")
connection_socket, address = server_socket.accept()
print("Client {} connected".format(address))
text= '''HTTP/1.1 200 OK
Via: HTTP/1.1 proxy_server_name
Server: Apache/1.3
Content-type: text/html, text, plain
Content-length: 95

<html>
<head>
<title>HTTP/1.1</TITLE>


</head>
<body>
<p>Client {} connected</p>
</body>
</html>'''.format(address)
text= "HTTP/1.1 200 OK\nContent-Length: {}\nContent-Type: text/plain; charset=utf-8\n\n{}".format(len(str(address)),address)

send_text(connection_socket, text)

server_socket.close()
connection_socket.close()


