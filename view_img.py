#!/usr/bin/env python3
from fl_networking_tools import ImageViewer
import socket
import pickle

udp_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_server.bind(("0.0.0.0", 20001))


viewer = ImageViewer()

def get_pixel_data():
    lost_pixels= 0
    last_pixel_updated= (-1, -1)
    while True:
        data, client_address = udp_server.recvfrom(1024)
        message = pickle.loads(data)
        pos = message[0]
        if (pos[1] - last_pixel_updated[1] > 1 or pos[0] - last_pixel_updated[0] > 1):
            lost_pixels+= 1
            viewer.text= lost_pixels
        last_pixel_updated= pos
        rgba = message[1]
        viewer.put_pixel(pos, rgba)

viewer.start(get_pixel_data)


