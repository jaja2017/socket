#!/usr/bin/env python3
import socket
import re
import sys
from threading import Thread

def send_text(sending_socket, text):
    text = text + "\n"
    data = text.encode()
    sending_socket.send(data)

def get_text(receiving_socket):
    buffer = ""
    socket_open = True
    while socket_open:
        # read any data from the socket
        data = receiving_socket.recv(1024)
        # if no data is returned the socket must be closed
        if not data:
            socket_open = False
        # add the data to the buffer
        buffer = buffer + data.decode()
        # is there a terminator in the buffer
        terminator_pos = buffer.find("\n")
        # if the value is greater than -1, a \n must exist
        while terminator_pos > -1:
            # get the message from the buffer
            message = buffer[:terminator_pos]
            # remove the message from the buffer
            buffer = buffer[terminator_pos + 1:]
            # yield the message (see below)
            yield message
            # is there another terminator in the buffer
            terminator_pos = buffer.find("\n")

def sanitised_input(prompt, type_=None, regexp_=None, min_=None, max_=None, range_=None):
    '''https://stackoverflow.com/questions/23294658/asking-the-user-for-input-until-they-give-a-valid-response'''
    if min_ is not None and max_ is not None and max_ < min_:
        raise ValueError("min_ must be less than or equal to max_.")
    while True:
        ui = input(prompt)
        if type_ is not None:
            try:
                ui = type_(ui)
            except ValueError:
                print("Input type must be {0}.".format(type_.__name__))
                continue
        if max_ is not None and ui > max_:
            print("Input must be less than or equal to {0}.".format(max_))
        elif min_ is not None and ui < min_:
            print("Input must be greater than or equal to {0}.".format(min_))
        elif range_ is not None and ui not in range_:
            if isinstance(range_, range):
                template = "Input must be between {0.start} and {0.stop}."
                print(template.format(range_))
            else:
                template = "Input must be {0}."
                if len(range_) == 1:
                    print(template.format(*range_))
                else:
                    expected = " or ".join((
                        ", ".join(str(x) for x in range_[:-1]),
                        str(range_[-1])
                    ))
                    print(template.format(expected))
        elif regexp_ is not None and not re.search(regexp_, ui):
            print("Input must fulfill s/{}/.".format(regexp_))
        else:
            return ui

def read_stdin():
    for line in sys.stdin:
        yield line

def sending(sending_socket):
    for line in read_stdin():
        send_text(sending_socket, line)
        
def receiving(receiving_socket):
    for line in get_text(receiving_socket):
        print(line)

if __name__ == '__main__':
    if 'client' == sanitised_input('Am I act as server or client? >', str.lower, range_=('server','client')):
        print('acting as client')
        server= False
        connection_socket= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print('created')
        adresse= sanitised_input('Servers address? >', str.lower, r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
        connection_socket.connect((adresse, 8081))
        print('connected')
    else:
        print('acting as server')
        server= True
        ''' AF_INET specifies Internet Protocol version 4 and SOCK_STREAM denotes TCP, so a TCP/IP socket is created'''
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print('created')
        '''all network IP addresses available on this computer connecting the port - use 127.0.0.1 for only loopback access'''
        server_socket.bind(("0.0.0.0", 8081))
        print('bound to 0.0.0.0:8081')
        server_socket.listen()
        print('listen')
        connection_socket, address = server_socket.accept()
        print("Client {} connected".format(address))
        print('accept')
    '''https://stackoverflow.com/questions/2957116/make-2-functions-run-at-the-same-time'''
    t1= Thread(target = sending(connection_socket))
    t1.start()
    print('send')
    t2= Thread(target = receiving(connection_socket))
    t2.start()
    print('recv')
    print('waiting for EOF input')
    t1.join()
    if server:
        server_socket.close()
        print('close server')
    connection_socket.close()
    print('close connection')

