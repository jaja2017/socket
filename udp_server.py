#!/usr/bin/env python3
import socket
from time import time
from platform import node, python_version_tuple
import json

current_time = time()
network_name = node()
python_version = python_version_tuple()

msg= json.dumps({"current_time": current_time, "network_name": network_name, "python_version": python_version})

'''type is set as socket.SOCK_DGRAM for UDP'''
udp_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_server.bind(("0.0.0.0", 20001))
print('waiting for contact on 0.0.0.0:20001')
data, client_address = udp_server.recvfrom(1024)
print('recv "{}" from udp://{}:{}'.format(data.decode(), *client_address))


udp_server.sendto(msg.encode(), client_address)
print('send "{}" back'.format(msg))
