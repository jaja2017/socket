#!/usr/bin/env python3
import socket
# Import the functions from the networking tools module
from fl_networking_tools import get_binary, send_binary
from chat_app import sanitised_input
import pickle

'''
Responses
LIST YOUR RESPONSE CODES HERE

2 - OK
3 - FAIL
4 - Question

6 - EoQ
9 - 3*FAIL
'''

# ask user team name and quiz server IP
adresse= sanitised_input('Servers address? >', str.lower, r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
name= sanitised_input('Team name? >', str, r'^\w{3,}')

# A flag used to control the quiz loop.
playing = True

quiz_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

quiz_server.connect((adresse, 2065))

# Use the team name to welcome the user to the quiz
send_binary(quiz_server, ["JOIN", name])

##if pickle.loads(quiz_server.recv(1024)[4:5]) == 2:
print("Wellcome team {}!".format(name))
##else:
##    print("JOIN failed!")
# Sending a command to the server.
send_binary(quiz_server, ["QUES", ""])

while playing:
    # The get_binary function returns a list of messages - loop over them
    for response in get_binary(quiz_server):
        # response is the command/response tuple - response[0] is the code
        if response[0] == 4: # The question response
            # Display it to the user.
            print(response[1])
            answer= sanitised_input('Your answer? >', str.lower, r'^\w{1,}')
            send_binary(quiz_server, ["EVAL", answer])
        elif response[0] == 2:
            print('You are right.')
        elif response[0] == 3:
            print("That's wrong.")
        elif response[0] == 6:
            print("Gatulation, End of Questions reached.")
            playing= False
            # break
        elif response[0] == 9:
            print("Sorry, 3rd Time you failed. Game Over.")
            playing= False
            # break
send_binary(quiz_server, ["BYE", ""])
quiz_server.close()